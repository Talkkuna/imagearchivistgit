﻿//This code is presented here: http://blogs.msdn.com/b/dditweb/archive/2007/08/22/speeding-up-image-loading-in-wpf-using-thumbnails.aspx

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.IO;
using Microsoft.WindowsAPICodePack.Shell;

namespace ImageArchivistWPF.Converters
{
    public class UriToBitmapConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ShellFile.FromFilePath(value.ToString()).Thumbnail.BitmapSource;

            /*
            //first check if thumbnail (=just a smaller image) already exists in thumbnails folder
            string thumbnailPath = getThumbnailPath( value.ToString() );
            if (File.Exists(thumbnailPath ) )
            {
                return thumbnailPath;
            }
                //if it doesn't exist, create and save the thumbnail
            else
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.DecodePixelWidth = 100;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.UriSource = new Uri(value.ToString());
                bitmap.EndInit();
                return bitmap;
            }*/
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        //add folder name "thumbnails" in the path before the file name
        private string getThumbnailPath(string imagePath )
        {
            var pathList = imagePath.Split('\\');
            string fileName = pathList.Last();
            pathList[pathList.Count() - 1] = "thumbnails";
            pathList[pathList.Count() - 1] = fileName;
            return String.Join("\\", pathList);
        }
    }
}
