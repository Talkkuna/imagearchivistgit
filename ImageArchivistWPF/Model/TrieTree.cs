﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageArchivistWPF.Model
{

    public static class TrieTree
    {

        private static TrieNode root_;

        static TrieTree()
        {
            //create root node
            root_ = new TrieNode(' ', true);
        }


        public static void AddWordToTrie(string word, MyImage image)
        {
            if(word == null || word == "" || image == null)
            {
                return;
            }

            word = word.ToLower();

            //add/set node for every letter
            TrieNode parent = root_;
            bool isLastLetter = false;
            for (int i = 0; i < word.Count(); ++i)
            {
                if (i == word.Count() - 1)
                {
                    isLastLetter = true;
                }
                parent = AddLetter(word.ElementAt(i), parent, isLastLetter);
            }

            //if node doesn't have this image already, add the image
            if (!parent.Contains(image))
            {
                parent.Images.Add(image);
            }
        }

        public static void AddToRoot(MyImage image)
        {
            root_.Images.Add(image);
        }

        public static HashSet<MyImage> Search(string word)
        {
            //Search node that contains word. It and all its child nodes which have images are search results.

            word = word.ToLower();

            TrieNode parent = root_;

            //if empty string, all images are returned
            if (word != root_.Letter.ToString())
            {

                for (int i = 0; i < word.Count(); ++i)
                {
                    if (parent.Children.ContainsKey(word.ElementAt(i)))
                    {
                        parent = parent.Children[word.ElementAt(i)];
                    }
                    else
                    {
                        return new HashSet<MyImage>();    //no search results
                    }
                }
            }

            HashSet<MyImage> images = CollectResultImages(parent);
            return images;


        }

        private static TrieNode AddLetter(char letter, TrieNode parent, bool lastLetter)
        {
            TrieNode node = null;

            //if node already exists, it is the new parent
            if (parent.Children.ContainsKey(letter))
            {
                node = parent.Children[letter];

                //set word existent
                if (lastLetter)
                {
                    node.Exists = true;
                }
            }
            else //if node doesn't exist, create new one
            {
                node = new TrieNode(letter, lastLetter);
                parent.Children.Add(letter, node);
            }

            return node;
        }

        private static HashSet<MyImage> CollectResultImages(TrieNode node)
        {
            //collect images of this node
            //use Set to prevent duplicates of same image

            HashSet<MyImage> images = new HashSet<MyImage>();
            if (node.Images.Count > 0)
            {
                images.UnionWith(node.Images);
            }

            //recursively go throug child nodes and collect images
            if (node.Children.Count > 0)
            {
                foreach (TrieNode child in node.Children.Values)
                {
                    images.UnionWith(CollectResultImages(child));
                }
            }

            return images;
        }

    }
}
