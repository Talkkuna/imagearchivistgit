﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ImageArchivistWPF.Model
{
    public class SaveableData
    {
        public ObservableCollection<MyImage> Images { get; set; }
        public ObservableCollection<Category> Categories { get; set; }
        public Options Options { get; set; }
    }
}
