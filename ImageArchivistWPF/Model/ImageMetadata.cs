﻿
using System.Collections.Generic;

namespace ImageArchivistWPF.Model
{
	public class ImageMetadata : NotifyPropertyChanged
	{
		public string Title { get; set; }
		public string Subject { get; set; }
		public List<string> Keywords { get; set; }
		public string Comment { get; set; }
	}
}
