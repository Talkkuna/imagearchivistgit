﻿
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using ImageArchivistWPF.ViewModel;

namespace ImageArchivistWPF.Model
{
	public class MyImage : NotifyPropertyChanged, IEquatable<MyImage>
	{

		#region Properties and variables

		private string name;
		public string Name
		{
			get { return name; }
			set
			{
				name = value;
				OnPropertyChanged();
			}
		}

		public string FilePath => $"{MainViewModel.DirPath}\\{Name}";

		[NonSerialized]
		private BitmapImage thumbnailImage;

		[NonSerialized]
		private ImageSource thumbnail;  
		[XmlIgnore]
		public ImageSource Thumbnail
		{
			get
			{
				return thumbnail;
			}
			set
			{
				if (thumbnail != value)
				{
					thumbnail = value;
					OnPropertyChanged();
				}
			}
		}

		private string description;
		public string Description
		{
			get { return description; }
			set
			{
				description = value;
				OnPropertyChanged();
			}
		}

		private DateTime? date;
		public DateTime? Date
		{
			get { return date; }
			set
			{
				date = value;
				OnPropertyChanged();
			}
		}

		private Category category;
		public Category Category
		{
			get { return category; }
			set
			{
				category = value;
				OnPropertyChanged();
			}
		}

		private ObservableCollection<Tag> tags;
		public ObservableCollection<Tag> Tags
		{
			get { return tags; }
			set
			{
				tags = value;
				OnPropertyChanged();
			}
		}

		private ImageMetadata imageMetadata;
		public ImageMetadata ImageMetadata
		{
			get { return imageMetadata; }
			set
			{
				imageMetadata = value;
				OnPropertyChanged();
			}
		}

		#endregion Properties and variables

		#region Constructors

		public MyImage()
		{
			Tags = new ObservableCollection<Tag>();
			description = "";
		}

		public MyImage(string filePath)
		{
			Tags = new ObservableCollection<Tag>();

			Name = ParseNameFromPath(filePath);
			thumbnailImage = null;
			thumbnail = null;
			description = "";
			ImageMetadata = ReadImageMetaData(FilePath);
		}

		private ImageMetadata ReadImageMetaData( string filePath )
		{
			var imageMetadata = new ImageMetadata();

			// open a filestream for the file we wish to look at
			using (Stream fs = File.Open(filePath, FileMode.Open, FileAccess.Read))
			{
				// create a decoder to parse the file
				BitmapDecoder decoder = BitmapDecoder.Create(fs, BitmapCreateOptions.None, BitmapCacheOption.Default);

				// grab the bitmap frame, which contains the metadata
				BitmapFrame frame = decoder.Frames[0];

				if ( frame.Metadata is BitmapMetadata metadata )
				{
					imageMetadata.Title = metadata.Title;
					imageMetadata.Subject = metadata.Subject;
					imageMetadata.Keywords = metadata.Keywords.ToList();
					imageMetadata.Comment = metadata.Comment;
				}
			}

			return imageMetadata;
		}

		public MyImage(MyImage original)
		{
			this.Category = original.Category;
			this.Date = original.Date;
			this.Description = original.Description;
			this.Name = original.Name;
			this.ImageMetadata = original.ImageMetadata;

			this.Tags = new ObservableCollection<Tag>();
			foreach (Tag tag in original.Tags)
			{
				this.Tags.Add(new Tag(tag));
			}
		}

		#endregion Constructors

		#region Public functions

		public void InitThumbnail()
		{
			try
			{
				if (thumbnailImage == null && FilePath != null && File.Exists(FilePath))
				{
					thumbnailImage = new BitmapImage();
					thumbnailImage.BeginInit();
					thumbnailImage.DecodePixelWidth = 100;
					thumbnailImage.CacheOption = BitmapCacheOption.OnLoad;
					thumbnailImage.UriSource = new Uri(FilePath);
					thumbnailImage.EndInit();
					thumbnailImage.Freeze();

					if (Application.Current != null)
					{
						Application.Current.Dispatcher.BeginInvoke((Action)delegate
						{
							Thumbnail = thumbnailImage;
						});
					}
				}
			}
			catch (Exception e)
			{
				thumbnailImage = null;
				App.Logger.Error($"Thumbnail initialization failed for image with file path {FilePath}", e);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="tagName"></param>
		/// <returns>Returns the tag that was added</returns>
		public Tag AddTag(string tagName)
		{
			var foundTag = Tags.FirstOrDefault( x => x.Text == tagName );

			//tag doesn't exist in this image so create it
			if (foundTag == null)
			{
				var newTag = new Tag(tagName);
				Tags.Add(newTag);
				return newTag;
			}
			//increase tag count of existing tag
			else
			{
				++foundTag.Count;
				return foundTag;
			}
		}

		public void RemoveTag(Tag tagToDelete)
		{
			Tags.Remove(tagToDelete);
		}

		public bool Contains( string text, bool isCaseSensitive)
		{
			string name = this.name;
			string description = this.description;

			if(!isCaseSensitive)
			{
				text = text.ToLower();
				name = name.ToLower();
				description = description.ToLower();
			}

			if( name.Contains(text) || description.Contains(text) || TagsContain(text, isCaseSensitive))
			{
				return true;
			}

			return false;
		}

		#endregion Public functions

		#region Private functions

		private bool TagsContain(string text, bool isCaseSensitive)
		{
			foreach( Tag tag in tags)
			{
				string tagText = tag.Text;
				if (!isCaseSensitive)
				{
					tagText = tagText.ToLower();
				}

				if (tagText.Contains(text))
				{
					return true;
				}
			}

			return false;
		}

		//Returns the name of the image, that is, the name of the file without the whole path.
		private string ParseNameFromPath(string path)
		{
			return path.Split('\\').Last();
		}

		public override string ToString()
		{
			return $"Name: {Name}";
		}

		public bool Equals(MyImage other)
		{
			if (this.Category?.Name == other.Category?.Name
			  && this.Description == other.Description
			  && this.Date == other.Date
			  && this.FilePath == other.FilePath
			  && this.Name == other.Name
			  && this.Tags.Select(tag => tag.Text).SequenceEqual(other.Tags.Select(tag => tag.Text))
			  && this.Tags.Select(tag => tag.Count).SequenceEqual(other.Tags.Select(tag => tag.Count))
			 )
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		#endregion Private functions
	}
}
