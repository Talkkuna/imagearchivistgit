﻿
using System;
using static ImageArchivistWPF.ViewModel.MainViewModel;

namespace ImageArchivistWPF.Model
{
    public class Options : NotifyPropertyChanged
    {
        public event EventHandler<BoolChangedEventArgs> TagAmountEnabledChanged;

        private bool isTagAmountEnabled;
        public bool IsTagAmountEnabled
        {
            get { return isTagAmountEnabled; }
            set
            {
                isTagAmountEnabled = value;
                OnPropertyChanged();
                TagAmountEnabledChanged?.Invoke(this, new BoolChangedEventArgs { NewValue = value });

            }
        }

        public Options()
        {
            IsTagAmountEnabled = true;
        }
    }
}
