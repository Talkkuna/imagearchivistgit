﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageArchivistWPF.Model
{
    public class TrieNode
    {

        private char letter_;
        public char Letter
        {
            get { return letter_; }
            set { letter_ = value; }
        }

        //tells if the word from the parents + this char exists
        private bool exists_;
        public bool Exists
        {
            get { return exists_; }
            set { exists_ = value; }
        }

        //images that contain the word
        private List<MyImage> images_;
        public List<MyImage> Images
        {
            get { return images_; }
            set { images_ = value; }
        }


        //child nodes of this node
        private Dictionary<char, TrieNode> children_;
        public Dictionary<char, TrieNode> Children
        {
            get { return children_; }
            set { children_ = value; }
        }

        public TrieNode()
        {
            Images = new List<MyImage>();
            Children = new Dictionary<char, TrieNode>();
        }

        public TrieNode(char letter, bool wordExists)
        {
            Letter = letter;
            Exists = wordExists;
            Images = new List<MyImage>();
            Children = new Dictionary<char, TrieNode>();
        }


        public bool Contains(MyImage image)
        {
            foreach (MyImage i in images_)
            {
                if (i == image)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
