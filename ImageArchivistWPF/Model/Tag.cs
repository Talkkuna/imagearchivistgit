﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImageArchivistWPF.Model
{
    public class Tag : NotifyPropertyChanged
    {
        public static List<string> AllTags = new List<string>();    //tag names

        private string text;
        public string Text
        {
            get { return text; }
            set
            {
                AddTagName(value);

                text = value;
                OnPropertyChanged();
            }
        }

        private uint count;
        public uint Count
        {
            get { return count; }
            set
            {
                count = value;
                OnPropertyChanged();
            }
        }

        public Tag()
        {
            Count = 1;
        }

        public Tag(string text)
        {
            Text = text;
            Count = 1;
        }

        public Tag(Tag tag)
        {
            Text = tag.Text;
            count = tag.Count;
        }

        #region Functions for the AllTags static property

        public static void AddTagName(string tagName)
        {
            if (!String.IsNullOrEmpty(tagName))
            {
                string existingTag = Tag.AllTags.FirstOrDefault(existingTagName => existingTagName == tagName);

                if (existingTag == null)
                {
                    Tag.AllTags.Add(tagName);
                }
            }
        }

        #endregion
    }
}
