﻿using System.Reflection;
using System.Timers;
using System.Windows;
using System.Windows.Threading;
using ImageArchivistWPF.Services;
using log4net;
using log4net.Config;

namespace ImageArchivistWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static Timer aTimer;

        //don't show error report multiple times to user if more exceptions are thrown.
        private bool reportDialogShownAlready = false;

        public App()
        {
            //For log4net logging
            XmlConfigurator.Configure();

            Logger.Info("\n\n----------------------------------------------------------------------------------------");
            Logger.Info("Image Archivist started.");

            this.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(App_DispatcherUnhandledException);

            SetTimer();
        }

        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            if (!reportDialogShownAlready)
            {
                var reportErrorDialog = new Views.DialogReportError("Error in Image Archivist", e.Exception);
                reportErrorDialog.ShowDialog();
                reportDialogShownAlready = true;
            }
        }

        private static void SetTimer()
        {
            aTimer = new Timer(60000);  //every minute
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            FileChangeChecker.StartUpdateFromDisk();
        }
    }
}
