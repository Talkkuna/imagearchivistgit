﻿using GalaSoft.MvvmLight.CommandWpf;
using ImageArchivistWPF.Model;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.Collections.Generic;

namespace ImageArchivistWPF.ViewModel
{
    public class CategoriesViewModel : NotifyPropertyChanged
    {
        private List<Category> categoriesUsedOnImages;

        #region Properties

        private ObservableCollection<Category> categories;
        public ObservableCollection<Category> Categories
        {
            get
            {
                return categories;
            }
            set
            {
                categories = value;
                OnPropertyChanged();
            }
        }

        private string inputCategory;
        public string InputCategory
        {
            get
            {
                return inputCategory;
            }
            set
            {
                inputCategory = value;
                OnPropertyChanged();
            }
        }

        #endregion Properties

        #region Commands

        public ICommand CreateCategoryCommand  { get; private set; }
        public ICommand DeleteCategoryCommand { get; private set; }

        #endregion Commands

        public CategoriesViewModel()
        {
            categoriesUsedOnImages = new List<Category>();

            Categories = new ObservableCollection<Category>();
            CreateCategoryCommand = new RelayCommand(CreateCategory);
            DeleteCategoryCommand = new RelayCommand<object>(DeleteCategory);
        }

        public void SetUsedCategories(List<Category> usedCategories)
        {
            categoriesUsedOnImages = usedCategories;
        }

        private void CreateCategory()
        {
            if (string.IsNullOrWhiteSpace(InputCategory))
            {
                MessageBox.Show("Write category name in the box above.");
                return;
            }

            if (Categories.Any(category => category.Name == InputCategory))
            {
                MessageBox.Show(String.Format("There is already a category \"{0}\"", InputCategory));
                return;
            }

            Categories.Add(new Category() { Name = InputCategory });
            InputCategory = String.Empty;

            MainViewModel.HasUnsavedChanges = true;
        }

        private void DeleteCategory(object itemToDelete)
        {

            if(!(itemToDelete is Category))
            {
                MessageBox.Show("Could not delete item. Contact developer.");
                return;
            }

            var category = itemToDelete as Category;

            if (categoriesUsedOnImages.Any(used => used.Name == category.Name))
            {
                MessageBox.Show("Cannot delete category. Category is used on some image(s).");
                return;
            }

            Categories.Remove(category);

            MainViewModel.HasUnsavedChanges = true;
        }
    }
}
