﻿using ImageArchivistWPF.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ImageArchivistWPF.ViewModel
{
    public class ImagesViewModel : NotifyPropertyChanged
    {
        #region Variables and properties

        public List<string> CreatedImagePaths = new List<string>();

        private readonly Object imagesWithUnloadedThumbnailsLock = new Object();

        private ObservableCollection<MyImage> images;
        public ObservableCollection<MyImage> Images
        {
            get
            {
                return images;
            }
            set
            {
                images = value;
                OnPropertyChanged();
            }
        }

        //public List<MyImage> FilteredImages = new List<MyImage>();

        private List<MyImage> imagesWithUnloadedThumbnails;

        private string searchPhrase;
        public string SearchPhrase
        {
            get
            {
                return searchPhrase;
            }
            set
            {
                searchPhrase = value;
                OnPropertyChanged();
            }
        }

        private bool isCaseSensitive;
        public bool IsCaseSensitive
        {
            get
            {
                return isCaseSensitive;
            }
            set
            {
                isCaseSensitive = value;
                OnPropertyChanged();
                OnPropertyChanged("SearchPhrase");
            }
        }

        private bool showUntagged;
        public bool ShowUntagged
        {
            get
            {
                return showUntagged;
            }
            set
            {
                showUntagged = value;
                OnPropertyChanged();
                OnPropertyChanged("SearchPhrase");
            }
        }

        private bool showUncategorized;
        public bool ShowUncategorized
        {
            get
            {
                return showUncategorized;
            }
            set
            {
                showUncategorized = value;
                OnPropertyChanged();
                OnPropertyChanged("SearchPhrase");
            }
        }

        private Category selectedCategory;
        public Category SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                selectedCategory = value;
                OnPropertyChanged();

            }
        }

        private ObservableCollection<Category> categories;
        public ObservableCollection<Category> Categories
        {
            get { return categories; }
            set
            {
                categories = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public ImagesViewModel()
        {
            Images = new ObservableCollection<MyImage>();
            imagesWithUnloadedThumbnails = new List<MyImage>();
        }

        //Creates a model for every image in the selected dictionary
        //if there is an xml in the folder create images from that data and then check for new images in folder
        public async Task ReadImagesAsync(string directory)
        {
            App.Logger.Info("Starting to ReadImagesAsync");

            var imageFiles = Directory.EnumerateFiles(directory, "*", SearchOption.TopDirectoryOnly);

            imagesWithUnloadedThumbnails = new List<MyImage>(Images);   //add serialized images

            await Task.Factory.StartNew(() => Parallel.ForEach(imageFiles, /*new ParallelOptions { MaxDegreeOfParallelism = 5 },*/ CreateImage));

            await Task.Factory.StartNew(() => LoadThumbnails(), TaskCreationOptions.LongRunning);

            App.Logger.Info("Finished ReadImagesAsync");
        }

        public void PrioritizeThumbnail( MyImage image /*string name*/)
        {
            if (imagesWithUnloadedThumbnails != null && imagesWithUnloadedThumbnails.Count > 0)
            {

                //var image = imagesWithUnloadedThumbnails_.FirstOrDefault(i => i.Name == name);
                if (image != null)
                {
                    lock (imagesWithUnloadedThumbnailsLock)
                    {
                        imagesWithUnloadedThumbnails.Remove(image);
                        imagesWithUnloadedThumbnails.Insert(0, image);
                    }
                }
            }
        }

        public void InitializeCategories(ObservableCollection<Category> categories)
        {
            //let's not use the same objects but create a new ones for this viewmodel as we don't want any changes to the original categories
            Categories = new ObservableCollection<Category>();
            Category emptyCategory = new Category();
            Categories.Add(emptyCategory);   //empty selection for user to not use category search

            foreach (Category category in categories)
            {
                Categories.Add(category);
            }
            SelectedCategory = emptyCategory;
        }

        public void CreateImage(string file)
        {
            //if image was not created in deserialization create a new image
            if (!CreatedImagePaths.Contains(file))
            {

                if (file.EndsWith(Constants.JPG) || file.EndsWith(Constants.JPEG) || file.EndsWith(Constants.PNG))
                {
                    MyImage image = new MyImage(file);
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        Images.Add(image);
                        CreatedImagePaths.Add(file);
                    });

                    imagesWithUnloadedThumbnails.Add(image);

                }
            }
        }

        public void LoadThumbnails()
        {
            MyImage image = null;

            try
            {
                //load first in "queue"
                while (imagesWithUnloadedThumbnails != null && imagesWithUnloadedThumbnails.Count > 0)
                {
                    lock (imagesWithUnloadedThumbnailsLock)
                    {
                        image = imagesWithUnloadedThumbnails.First();
                        imagesWithUnloadedThumbnails.RemoveAt(0);
                    }
                    image.InitThumbnail();
                }
            }
            catch (Exception e)
            {
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    string imageHasValue = image == null ? "Image null" : "Image exists";
                    App.Logger.Error($"imagesWithUnloadedThumbnails: {imagesWithUnloadedThumbnails?.Count}. {imageHasValue}. image: {image?.Name} {image?.FilePath}", e);
                });
                throw e;
            }
        }

        public MyImage GetPreviousImage(MyImage currentImage, List<MyImage> filteredImages)
        {
            int previousIndex = filteredImages.IndexOf(currentImage) - 1;
            if (previousIndex < 0)
            {
                previousIndex = filteredImages.Count - 1;   //last image
            }

            return filteredImages[previousIndex];
        }

        public MyImage GetNextImage(MyImage currentImage, List<MyImage> filteredImages)
        {
            int nextIndex = filteredImages.IndexOf(currentImage) + 1;
            if (nextIndex >= filteredImages.Count)
            {
                nextIndex = 0;
            }

            return filteredImages[nextIndex];
        }

        public bool SearchFilter(object item)
        {
            bool contains = true;
            MyImage image = item as MyImage;

            //Don't show images which are removed from disk
            if(image.FilePath == null)
            {
                return false;
            }         

            //Filter by free text
            if (!String.IsNullOrWhiteSpace(SearchPhrase))
            {
                contains = image.Contains(SearchPhrase, IsCaseSensitive);
            }

            //Only images with no tags
            if (ShowUntagged && image.Tags.Any())
            {
                return false;
            }

            //Only images with no category
            if (ShowUncategorized)
            {
                if (image.Category != null && !String.IsNullOrEmpty(image.Category.Name))
                {
                    return false;
                }
            }
            //Filter by category
            else if ((SelectedCategory != null) && (SelectedCategory.Name != null) //a category is selected
                && ((image.Category == null) || ((image.Category != null) && (SelectedCategory.Name != image.Category.Name)))) //but image doesn't have category OR image has different category
            {
                return false;
            }

            //if (!FilteredImages.Contains(image)) { FilteredImages.Add(image); }

            return contains;
        }
    }




    // Sort number part numerically and alphabet part alphabetically
    public class CustomSorter : IComparer
    {
        public int Compare(object a, object b)
        {
            MyImage imageA = a as MyImage;
            MyImage imageB = b as MyImage;

            if (imageA == null || imageB == null)
            {
                return 0;
            }

            List<string> aInAlphaAndNumParts = SplitIntoAlphaAndNumParts(imageA.Name);
            List<string> bInAlphaAndNumParts = SplitIntoAlphaAndNumParts(imageB.Name);

            aInAlphaAndNumParts = RemoveFileEnding(aInAlphaAndNumParts);
            bInAlphaAndNumParts = RemoveFileEnding(bInAlphaAndNumParts);


            for (int i = 0; i < aInAlphaAndNumParts.Count; ++i)
            {
                //if there's no more parts in b, b is first
                if (i >= bInAlphaAndNumParts.Count)
                {
                    return 1;
                }

                string charactersA = aInAlphaAndNumParts.ElementAt(i);
                string charactersB = bInAlphaAndNumParts.ElementAt(i);
                bool aIsNumber = Int32.TryParse(charactersA, out int numberA);
                bool bIsNumber = Int32.TryParse(charactersB, out int numberB);

                if ((aIsNumber && bIsNumber && numberA < numberB) || (aIsNumber && !bIsNumber) || (!aIsNumber && !bIsNumber && (String.Compare(charactersA, charactersB, true) == -1)))
                {
                    return -1;
                }
                else if ((aIsNumber && bIsNumber && numberA > numberB) || (!aIsNumber && bIsNumber) || (!aIsNumber && !bIsNumber && (String.Compare(charactersA, charactersB, true) == 1)))
                {
                    return 1;
                }
            }

            //parts were same until this but if b still continues a is smaller
            if (aInAlphaAndNumParts.Count < bInAlphaAndNumParts.Count)
            {
                return -1;
            }

            //same strings
            return 0;
        }

        private List<string> SplitIntoAlphaAndNumParts(string text)
        {
            List<string> result = new List<string>();
            bool previousWasDigit = true;
            string part = "";
            for (int i = 0; i < text.Length; ++i)
            {
                bool isDigit = Char.IsDigit(text.ElementAt(i));
                if (i > 0 && ((!isDigit && previousWasDigit) || (isDigit && !previousWasDigit)))
                {
                    result.Add(part);
                    part = "";
                }

                previousWasDigit = isDigit;
                part += text.ElementAt(i);
            }
            result.Add(part);

            return result;
        }

        private List<string> RemoveFileEnding(List<string> list)
        {
            //remove file type from the end such as ".jpg". Note, there can be multiple dots in the file name.
            string[] last = list.Last().Split('.');
            string fileEnding = last.Last();
            list.RemoveAt(list.Count - 1);
            string lastPartWithoutFileEnding = "";
            for (int i = 0; i < last.Count() - 1; ++i)
            {
                lastPartWithoutFileEnding += last.ElementAt(i);
            }
            if (lastPartWithoutFileEnding != "")
            {
                list.Add(lastPartWithoutFileEnding);
            }

            return list;
        }
    }
}
