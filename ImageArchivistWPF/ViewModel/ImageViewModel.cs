﻿using GalaSoft.MvvmLight.CommandWpf;
using ImageArchivistWPF.Model;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using static ImageArchivistWPF.ViewModel.MainViewModel;

namespace ImageArchivistWPF.ViewModel
{
    public class ImageEventArgs : EventArgs
    {
        public MyImage Image { get; set; }
    }

    public class ImageViewModel : NotifyPropertyChanged
    {
        #region Events

        public event EventHandler Closed;
        public event EventHandler<ImageEventArgs> SetPreviousImageView;
        public event EventHandler<ImageEventArgs> SetNextImageView;

        #endregion Events

        #region Commands

        public ICommand CloseCommand { get; private set; }
        public ICommand RemoveDateCommand { get; private set; }
        public ICommand AddTagCommand { get; private set; }
        public ICommand RemoveTagCommand { get; private set; }
        public ICommand SelectTag { get; private set; }
        public ICommand IncreaseCountCommand { get; private set; }
        public ICommand DecreaseCountCommand { get; private set; }
        public ICommand PreviousImageCommand { get; private set; }
        public ICommand NextImageCommand { get; private set; }

        #endregion Commands

        #region Variables and properties

        private readonly MyImage originalImage;

        private MyImage image;
        public MyImage Image
        {
            get { return image; }
            private set
            {
                image = value;
                OnPropertyChanged();
            }
        }

        private Category selectedCategory;
        public Category SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                selectedCategory = value;
                Image.Category = value;
                OnPropertyChanged();
            }
        }

        public string NewTag
        {
            get { return SelectedExistingTag; }
            set
            {
                if (SelectedExistingTag != null)
                {
                    return;
                }
                if (!string.IsNullOrEmpty(value))
                {
                    SelectedExistingTag = value;
                }
            }
        }

        public ObservableCollection<string> ExistingTags { get; set; } = new ObservableCollection<string>();

        private string selectedExistingTag;
        public string SelectedExistingTag
        {
            get { return selectedExistingTag; }
            set
            {
                selectedExistingTag = value;
                OnPropertyChanged();
            }
        }

        private Tag selectedTag;
        public Tag SelectedTag
        {
            get { return selectedTag; }
            set
            {
                selectedTag = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Category> Categories { get; private set; }

        private bool showTagAmounts;
        public bool ShowTagAmounts
        {
            get { return showTagAmounts; }
            set
            {
                showTagAmounts = value;
                OnPropertyChanged();
            }
        }

        #endregion Properties

        public ImageViewModel(MyImage image, ObservableCollection<Category> categories)
        {
            CloseCommand = new RelayCommand(CloseView);
            RemoveDateCommand = new RelayCommand(RemoveDate);
            AddTagCommand = new RelayCommand(AddTag);
            RemoveTagCommand = new RelayCommand<object>(RemoveTag, ATagIsSelected);
            SelectTag = new RelayCommand<object>(SetSelectedTag);
            IncreaseCountCommand = new RelayCommand(IncreaseSelectedTagCount, ATagIsSelected);
            DecreaseCountCommand = new RelayCommand(DecreaseSelectedTagCount, IsGreaterThanMin);
            PreviousImageCommand = new RelayCommand(SetPreviousImage);
            NextImageCommand = new RelayCommand(SetNextImage);

            Image = image;
            originalImage = new MyImage(image);

            image.PropertyChanged += OnChangeSaveStatus;

            //add tag names which image doesn't have (for combobox)
            foreach (string existingTagName in Tag.AllTags)
            {
                if (!image.Tags.Any(tag => tag.Text == existingTagName))
                {
                    ExistingTags.Add(existingTagName);
                }
            }

            foreach(Tag tagOnImage in image.Tags)
            {
                tagOnImage.PropertyChanged += OnChangeSaveStatus;
            }

            InitializeCategories(categories);
        }

        public void SetShowTagAmounts(object sender, BoolChangedEventArgs e)
        {
            ShowTagAmounts = e.NewValue;
        }

        private void CloseView()
        {
            Closed?.Invoke(this, null);
        }

        private void RemoveDate()
        {
            Image.Date = null;
        }

        public void AddTag()
        {
            if (!String.IsNullOrWhiteSpace(SelectedExistingTag))
            {
                var newTag = Image.AddTag(SelectedExistingTag);
                newTag.PropertyChanged += OnChangeSaveStatus;
                ExistingTags.Remove(SelectedExistingTag);
                SelectedExistingTag = null;
                NewTag = "";
                OnPropertyChanged("NewTag");
                ChangeSaveStatus();
            }
        }

        private void RemoveTag(object tagToRemoveObject)
        {
            if (!(tagToRemoveObject is Tag))
            {
                return;
            }

            Tag tagToRemove = tagToRemoveObject as Tag;
            Image.RemoveTag(tagToRemove);
            SelectedTag = null;
            ChangeSaveStatus();
        }

        private void SetSelectedTag(object selectedTag)
        {
            if (selectedTag is Tag)
            {
                SelectedTag = Image.Tags.FirstOrDefault(tag => tag.Text == (selectedTag as Tag).Text);
            }
        }

        private void IncreaseSelectedTagCount()
        {
            if (SelectedTag != null)
            {
                SelectedTag.Count++;
            }
        }

        private void DecreaseSelectedTagCount()
        {
            if (SelectedTag != null)
            {
                SelectedTag.Count--;
            }
        }

        private bool ATagIsSelected()
        {
            return SelectedTag != null;
        }

        private bool ATagIsSelected(object selectedTag)
        {
            return ATagIsSelected();
        }

        private bool IsGreaterThanMin()
        {
            if (SelectedTag == null)
            {
                return false;
            }
            return SelectedTag.Count > 1;
        }

        private void SetPreviousImage()
        {
            SetPreviousImageView?.Invoke(this, new ImageEventArgs() { Image = Image });
        }

        private void SetNextImage()
        {
            SetNextImageView?.Invoke(this, new ImageEventArgs() { Image = Image });
        }

        private void InitializeCategories(ObservableCollection<Category> categories)
        {
            //let's not use the same objects but create a new ones for this viewmodel as we don't want any changes to the original categories
            Categories = new ObservableCollection<Category>();
            Category emptyCategory = new Category();
            Categories.Add(emptyCategory);   //empty selection for user to remove category from image

            foreach (Category category in categories)
            {
                Categories.Add(category);
            }
            SelectedCategory = image.Category == null ? emptyCategory : categories.FirstOrDefault(category => category.Name == image.Category.Name);
        }

        private void OnChangeSaveStatus(object sender, EventArgs e)
        {
            ChangeSaveStatus();
        }

        private void ChangeSaveStatus()
        {
            if (MainViewModel.HasUnsavedChanges != true &&
            originalImage != null && !originalImage.Equals(image))
            {
                MainViewModel.HasUnsavedChanges = true;
            }
        }
    }
}
