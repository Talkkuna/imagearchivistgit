﻿using ImageArchivistWPF.Model;

namespace ImageArchivistWPF.ViewModel
{
    public class OptionsViewModel : NotifyPropertyChanged
    {

        private Options options;
        public Options Options
        {
            get
            {
                return options;
            }
            set
            {
                options = value;
                OnPropertyChanged();
            }
        }
    }
}
