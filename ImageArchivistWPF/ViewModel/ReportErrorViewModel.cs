﻿using GalaSoft.MvvmLight.Command;
using ImageArchivistWPF.Services;
using System;
using System.Windows.Input;

namespace ImageArchivistWPF.ViewModel
{
    public class ReportErrorViewModel : NotifyPropertyChanged
    {
        public event EventHandler Closed;

        private string descriptionFromUser;
        public string DescriptionFromUser
        {
            get
            {
                return descriptionFromUser;
            }
            set
            {
                descriptionFromUser = value;
                OnPropertyChanged();
            }
        }

        public ICommand SendErrorReportCommand { get; private set; }

        private string errorMessage;
        private Exception exceptionToReport;

        public ReportErrorViewModel(string message, Exception exceptionToReport)
        {
            DescriptionFromUser = "";
            SendErrorReportCommand = new RelayCommand(SendErrorReport);
            this.errorMessage = message;
            this.exceptionToReport = exceptionToReport;
        }

        private void SendErrorReport()
        {
            ErrorReporter.SendErrorReport(errorMessage, DescriptionFromUser, this.exceptionToReport);
            Closed?.Invoke(this, null);
        }
    }
}
