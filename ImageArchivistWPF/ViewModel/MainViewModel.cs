using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using ImageArchivistWPF.Model;
using ImageArchivistWPF.View;
using ImageArchivistWPF.Views;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;

namespace ImageArchivistWPF.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public class BoolChangedEventArgs : EventArgs
        {
            public bool NewValue { get; set; }
        }

        #region Variables and Properties

        public CategoriesViewModel CategoriesViewModel;
        public static ImagesViewModel ImagesViewModel;
        public OptionsViewModel OptionsViewModel;

        public static bool HasUnsavedChanges = false;
        public static bool IsInitializing = true;

        public event EventHandler OnExitProgram;
        public event EventHandler OnDirectorySelected;

        public string Title
        {
            get { return $"Image Archivist {Constants.VERSION}"; }
        }

        private object imageContent;
        public object ImageContent
        {
            get { return imageContent; }
            set
            {
                imageContent = value;
                RaisePropertyChanged("ImageContent");
            }
        }

        public static string DirPath { get; set; }

        #endregion Variables and Properties

        #region Commands

        public ICommand ChooseDirCommand { get; private set; }
        public ICommand SaveXmlCommand { get; private set; }
        public ICommand CategoriesCommand { get; private set; }
        public ICommand OptionsCommand { get; private set; }
        public ICommand ShowAboutCommand { get; private set; }
        public ICommand ExitCommand { get; private set; }

        #endregion Commands

        public MainViewModel()
        {
            ChooseDirCommand = new RelayCommand(ChooseDir);
            SaveXmlCommand = new RelayCommand(SaveXml, IsFolderSelected);
            CategoriesCommand = new RelayCommand(OpenCategoryEditor, IsFolderSelected);
            OptionsCommand = new RelayCommand(OpenOptionsDialog, IsFolderSelected);
            ShowAboutCommand = new RelayCommand(ShowAbout);
            ExitCommand = new RelayCommand(ExitProgram);
        }

        //serialize read and modified data to xml
        public void SaveAllToXml(SaveableData data)
        {
            App.Logger.Info("SaveAllToXml starting");

            try
            {
                using (Stream stream = File.Open(DirPath + Constants.XML_FILE_NAME, FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SaveableData), new Type[] { typeof(MyImage), typeof(Category) });
                    serializer.Serialize(stream, data);
                }

                HasUnsavedChanges = false;
            }
            catch (IOException e)
            {
                App.Logger.Error("SaveAllToXml failed: ", e);
                MessageBoxResult result = MessageBox.Show("There was an error. Check that no file is open elsewhere.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            finally
            {
                App.Logger.Info("SaveAllToXml finished");
            }
        }

        public SaveableData DeserializeAllData()
        {
            SaveableData data = null;

            try
            {
                string filePath = DirPath + Constants.XML_FILE_NAME;
                if (File.Exists(filePath))
                {

                    using (Stream stream = File.Open(filePath, FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(SaveableData), new Type[] { typeof(MyImage), typeof(Category) });
                        data = (SaveableData)serializer.Deserialize(stream);
                    }
                }

                HasUnsavedChanges = false;
            }
            catch (Exception e)
            {
                App.Logger.Error("DeserializeAllData failed: ", e);
                MessageBox.Show($"There was an error in deserialization:\n{e.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return data;
        }

        public void InitializeViews(ImagesView imagesView)
        {
            ImagesViewModel = new ImagesViewModel();
            imagesView.DataContext = ImagesViewModel;

            CategoriesViewModel = new CategoriesViewModel();
            OptionsViewModel = new OptionsViewModel();
        }

        public void PrepareClosing(object sender, CancelEventArgs e)
        {
            if (MainViewModel.HasUnsavedChanges)
            {
                MessageBoxResult result = MessageBox.Show("You haven't saved your latest changes. Do you want to save them before exiting?", "Unsaved changes.", MessageBoxButton.YesNoCancel);


                if (result == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
                else if (result == MessageBoxResult.Yes)
                {
                    SaveAllToXml();
                }
            }
        }

        private async void ChooseDir()
        {
            App.Logger.Info("User is selecting a new folder.");

            IsInitializing = true;

            if (!HandlePreviousFolderBeforeOpeningNewOne())
            {
                IsInitializing = false;
                return;
            }

            var dialog = new CommonOpenFileDialog
            {
                Title = "Choose a folder where the images are located.",
                IsFolderPicker = true,

                AddToMostRecentlyUsedList = false,
                AllowNonFileSystemItems = false,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                Multiselect = false,
                ShowPlacesList = true
            };

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                ClearPreviousFolderSession();
                OnDirectorySelected?.Invoke(this, null);

                DirPath = dialog.FileName;
                SaveableData readData = DeserializeAllData();
                if (readData != null)
                {
                    ImagesViewModel.Images = readData.Images;
                    CategoriesViewModel.Categories = readData.Categories;
                    ImagesViewModel.CreatedImagePaths = readData.Images.Select(image => image.FilePath).ToList();

                    OptionsViewModel.Options = readData.Options;
                    if(OptionsViewModel.Options == null)
                    {
                        OptionsViewModel.Options = new Options();
                    }
                }

                RefreshCategories();
                await ImagesViewModel.ReadImagesAsync(dialog.FileName);
            }

            IsInitializing = false;

            App.Logger.Info($"New folder {DirPath} set up successfully.");
        }

        private void ClearPreviousFolderSession()
        {
            ImagesViewModel.Images.Clear();
            CategoriesViewModel.Categories.Clear();
            ImagesViewModel.CreatedImagePaths.Clear();
            OptionsViewModel.Options = new Options();
        }

        private void SaveXml()
        {
            SaveAllToXml();
            MessageBoxResult result = MessageBox.Show("Saved!", "Saved.", MessageBoxButton.OK);
        }

        private bool IsFolderSelected()
        {
            return !String.IsNullOrEmpty(DirPath);
        }

        private void OpenCategoryEditor()
        {
            var dialog = new DialogCategories
            {
                DataContext = CategoriesViewModel
            };
            CategoriesViewModel.SetUsedCategories(ImagesViewModel.Images.Select(image => image.Category).Where(category => category != null && category.Name != null).GroupBy(category => category.Name).Select(group => group.First()).ToList());

            dialog.ShowDialog();

            RefreshCategories();
        }

        private void OpenOptionsDialog()
        {
            var dialog = new DialogOptions
            {
                DataContext = OptionsViewModel
            };
            dialog.ShowDialog();
        }

        private void ShowAbout()
        {
            MessageBox.Show(String.Format("Image Archivist {0}\n\nTaina Peltola\nwww.codeartist.fi", Constants.VERSION));
        }

        private void ExitProgram()
        {
            OnExitProgram?.Invoke(this, null);
        }

        private bool HandlePreviousFolderBeforeOpeningNewOne()
        {
            if (DirPath != null)
            {
                if (MainViewModel.HasUnsavedChanges)
                {
                    string message = "You haven't saved changes in previous folder. Do you want to save?";
                    App.Logger.Info($"Showing message to user: {message}");
                    MessageBoxResult saveResult = MessageBox.Show(message, "Unsaved changes", MessageBoxButton.YesNoCancel);
                    if (saveResult == MessageBoxResult.Yes)
                    {
                        SaveAllToXml();
                        MessageBoxResult result = MessageBox.Show("Saved!", "Saved.", MessageBoxButton.OK);
                    }
                    else if (saveResult == MessageBoxResult.Cancel)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void SaveAllToXml()
        {
            SaveAllToXml(new SaveableData() { Images = ImagesViewModel.Images, Categories = CategoriesViewModel.Categories, Options = OptionsViewModel.Options });
        }

        private void RefreshCategories()
        {
            ImagesViewModel.InitializeCategories(CategoriesViewModel.Categories);
        }
    }
}