﻿using ImageArchivistWPF.Model;
using ImageArchivistWPF.ViewModel;
using System;
using System.Windows;
using System.Windows.Data;
using System.Linq;
using ImageArchivistWPF.View;
using System.Windows.Media;

namespace ImageArchivistWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private MainViewModel mainViewModel;
        private ImageViewModel currentImageViewModel;

        public MainWindow()
        {
            InitializeComponent();

            this.mainViewModel = new MainViewModel();
            this.DataContext = mainViewModel;
            this.mainViewModel.ImageContent = null;
            currentImageViewModel = null;

            mainViewModel.InitializeViews(this.ImagesView);

            this.ImagesView.ImageOpened += SetImageView;

            this.Closing += mainViewModel.PrepareClosing;
            mainViewModel.OnExitProgram += Exit;
            mainViewModel.OnDirectorySelected += SetImagesViewActive;

        }

        private void SetImageView(object sender, ImageEventArgs e)
        {
            App.Logger.Info($"Activating Image View for image: {e.Image?.Name}");

            ImageView imageView = GetControlByName(this.ImageContent, "ImageView") as ImageView;
            if (imageView != null)
            {
                imageView.UndoZoom();
            }

            currentImageViewModel = new ImageViewModel(e.Image, mainViewModel.CategoriesViewModel.Categories);
            mainViewModel.ImageContent = currentImageViewModel;

            currentImageViewModel.Closed += this.SetImagesViewActive;
            currentImageViewModel.SetPreviousImageView += SetPreviousImageView;
            currentImageViewModel.SetNextImageView += SetNextImageView;
            mainViewModel.OptionsViewModel.Options.TagAmountEnabledChanged += currentImageViewModel.SetShowTagAmounts;
            currentImageViewModel.ShowTagAmounts = mainViewModel.OptionsViewModel.Options.IsTagAmountEnabled;

            this.ImagesView.IsEnabled = false;
            this.ImagesView.Visibility = Visibility.Hidden;

            this.ImageContent.IsEnabled = true;
            this.ImageContent.Visibility = Visibility.Visible;
        }

        private void SetPreviousImageView(object sender, ImageEventArgs e)
        {
            CollectionViewSource source = (CollectionViewSource)(ImagesView.Resources["Images"]);
            var filteredImages = source.View.OfType<MyImage>().ToList();
            SetImageView(this, new ImageEventArgs() { Image = MainViewModel.ImagesViewModel.GetPreviousImage(e.Image, filteredImages) });
        }

        private void SetNextImageView(object sender, ImageEventArgs e)
        {
            CollectionViewSource source = (CollectionViewSource)(ImagesView.Resources["Images"]);
            var filteredImages = source.View.OfType<MyImage>().ToList();
            SetImageView(this, new ImageEventArgs() { Image = MainViewModel.ImagesViewModel.GetNextImage(e.Image, filteredImages) });
        }

        private void SetImagesViewActive(object sender, EventArgs e)
        {
            App.Logger.Info("Activating Images View");

            currentImageViewModel = null;
            this.mainViewModel.ImageContent = null;
            this.ImageContent.IsEnabled = false;
            this.ImageContent.Visibility = Visibility.Hidden;

            this.ImagesView.IsEnabled = true;
            this.ImagesView.Visibility = Visibility.Visible;
        }

        private void Exit(object sender, EventArgs e)
        {
            this.Close();
        }

        public static FrameworkElement GetControlByName(DependencyObject parent, string name)
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (var i = 0; i < count; ++i)
            {
                var child = VisualTreeHelper.GetChild(parent, i) as FrameworkElement;
                if (child != null)
                {
                    if (child.Name == name)
                    {
                        return child;
                    }
                    var descendantFromName = GetControlByName(child, name);
                    if (descendantFromName != null)
                    {
                        return descendantFromName;
                    }
                }
            }
            return null;
        }
    }
}
