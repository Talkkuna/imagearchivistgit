﻿using ImageArchivistWPF.ViewModel;
using System;
using System.Windows;

namespace ImageArchivistWPF.Views
{
    /// <summary>
    /// Interaction logic for DialogReportError.xaml
    /// </summary>
    public partial class DialogReportError : Window
    {
        public DialogReportError(string message, Exception exceptionToReport)
        {
            InitializeComponent();

            var viewModel = new ReportErrorViewModel(message, exceptionToReport);
            this.DataContext = viewModel;
            viewModel.Closed += CloseDialog;
        }

        private void CloseDialog(object sender, EventArgs eventArgs)
        {
            this.Close();
        }
    }
}
