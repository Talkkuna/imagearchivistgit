﻿using ImageArchivistWPF.ViewModel;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ImageArchivistWPF.View
{
    /// <summary>
    /// Interaction logic for ImageView.xaml
    /// </summary>
    public partial class ImageView : UserControl
    {
        private const double MAX_SCALE = 10.0;
        private const double MIN_SCALE = 1.0;

        private Point start;
        private Point origin;

        private int zoomCounter = 0;

        public ImageView()
        {
            InitializeComponent();

            this.Loaded += SetUserControlFocus;
        }

        public void UndoZoom()
        {
            Matrix m = this.Image.RenderTransform.Value;

            for (int i = 0; i < zoomCounter; i++)
            {
                m.Scale(1.0 / 1.1, 1.0 / 1.1);
            }

            m.OffsetX = 0;
            m.OffsetY = 0;

            this.Image.RenderTransform = new MatrixTransform(m);
            zoomCounter = 0;
        }

        private void SetUserControlFocus(object sender, EventArgs e)
        {
            this.Focus();
        }

        //zoom image on mousewheel
        private void Image_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            var image = (Image)sender;
            Point p = e.MouseDevice.GetPosition(image);

            Matrix m = image.RenderTransform.Value;
            if (e.Delta > 0)
            {
                m.ScaleAtPrepend(1.1, 1.1, p.X, p.Y);

                if (m.M11 <= MAX_SCALE)
                {
                    image.RenderTransform = new MatrixTransform(m);
                    ++zoomCounter;
                }

            }
            else
            {
                //when zooming out, center image to keep it in the middle of the view        
                m.OffsetX -= m.OffsetX / (double)zoomCounter;
                m.OffsetY -= m.OffsetY / (double)zoomCounter;
                m.Scale(1.0 / 1.1, 1.0 / 1.1);

                if (m.M11 >= MIN_SCALE)
                {
                    image.RenderTransform = new MatrixTransform(m);
                    --zoomCounter;
                }
            }
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var image = (Image)sender;
            var imageBorder = (Border)image.Parent;

            if (image.IsMouseCaptured) return;
            image.CaptureMouse();

            start = e.GetPosition(imageBorder);
            origin.X = image.RenderTransform.Value.OffsetX;
            origin.Y = image.RenderTransform.Value.OffsetY;
        }

        private void Image_MouseMove(object sender, MouseEventArgs e)
        {
            var image = (Image)sender;
            var imageBorder = (Border)image.Parent;

            if (!image.IsMouseCaptured) return;
            Point p = e.MouseDevice.GetPosition(imageBorder);

            Matrix m = image.RenderTransform.Value;

            var tempOffsetX = origin.X + (p.X - start.X);
            if (tempOffsetX < 0.0f /*&& tempOffsetX > -imageBorder.ActualWidth / 2.0*/)
            {
                m.OffsetX = tempOffsetX;
            }


            var tempOffsetY = origin.Y + (p.Y - start.Y);
            if ( /*(tempOffsetY > image.ActualHeight * m.M22) &&*/ (tempOffsetY < 0.0f)) //TODO: How to get rendered height of the image before clipping??
            {
                m.OffsetY = tempOffsetY;
            }

            image.RenderTransform = new MatrixTransform(m);
        }

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var image = (Image)sender;
            image.ReleaseMouseCapture();
        }

        private void NewTagTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.AddTagButton.Focus();

                (this.DataContext as ImageViewModel).AddTag();
            }
        }
    }
}
