﻿using ImageArchivistWPF.Model;
using ImageArchivistWPF.ViewModel;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;

namespace ImageArchivistWPF.View
{
    /// <summary>
    /// Interaction logic for ThumbnailView.xaml
    /// </summary>
    public partial class ImagesView : UserControl
    {
        public event EventHandler<ImageEventArgs> ImageOpened;

        public ImagesView()
        {
            InitializeComponent();

            this.Loaded += ThumbnailView_Loaded;
        }

        private void ListBoxItem_MouseDoubleClickEvent(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem imageItem = sender as ListBoxItem;
            MyImage image = imageItem.Content as MyImage;

            ImageOpened?.Invoke(this, new ImageEventArgs { Image = image });
        }

        //mouse wheel scroll only one line/row at a time (instead of default: whole visible area at once)
        private void Thumbnails_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta < 0)
            {
                ScrollBar.LineDownCommand.Execute(null, e.OriginalSource as IInputElement);
            }
            else
            {
                ScrollBar.LineUpCommand.Execute(null, e.OriginalSource as IInputElement);
            }
            e.Handled = true;
        }


        private void ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            var offset = (int)e.VerticalOffset;
            int itemCountOnRow = (int)((e.ViewportWidth - 1) / (100));  //TODO: variable for thumbnailWidth and some paddings?? these values give only 1 pixel wrong result
            var firstIndex = itemCountOnRow * offset;
            var visibleItemCount = e.ViewportHeight * itemCountOnRow; //TODO: viewport height gives a weird number, a couple too big always, but too much doesn't really matter here

            //prioritize all visible thumbnails
            ImagesViewModel viewModel = this.DataContext as ImagesViewModel;
            Task.Factory.StartNew(() =>
            {
                try
                {
                    for (int i = firstIndex; (i < firstIndex + visibleItemCount) && (i < Thumbnails.Items.Count); ++i)
                    {
                        var image = Thumbnails.Items[i] as MyImage;
                        viewModel.PrioritizeThumbnail(image);
                    }
                }
                catch(Exception ex)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        App.Logger.Error("Prioritizing thumbnails failed.", ex);
                    });
                }
            });
        }

        private void ThumbnailView_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateImagesView();
        }

        private void SearchTextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateImagesView();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateImagesView();
        }

        private void UpdateImagesView()
        {
            CollectionViewSource source = (CollectionViewSource)(this.Resources["Images"]);
            ListCollectionView view = (ListCollectionView)source.View;
            ImagesViewModel viewModel = this.DataContext as ImagesViewModel;
            //viewModel.FilteredImages.Clear();
            view.CustomSort = new CustomSorter();
            view.Filter = viewModel.SearchFilter;
            view.Refresh();
        }

        private void CheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            UpdateImagesView();
        }
    }
}
