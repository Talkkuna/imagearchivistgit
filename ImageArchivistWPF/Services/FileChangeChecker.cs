﻿using ImageArchivistWPF.Model;
using ImageArchivistWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ImageArchivistWPF.Services
{
    public static class FileChangeChecker
    {

        public static void StartUpdateFromDisk()
        {
            try
            {

                App.Logger.Info($"Background image update from disk started. IsInitializing: {MainViewModel.IsInitializing}");

                if (!MainViewModel.IsInitializing)
                {
                    var task = new Task(UpdateFromDisk);
                    task.Start();
                    task.Wait();
                }

                App.Logger.Info("Background image update from disk finished.");
            }
            catch (Exception e)
            {
                //If background loading of images doesn't work we don't want the whole program to crash.
                App.Logger.Error("Background loading of images failed.", e);
            }
        }

        private static void UpdateFromDisk()
        {
            AddNewImages(GetFilesToAddToMemory().ToList());

            RemoveFilePaths(GetFilesToDeleteFromMemory().ToList());
        }

        private static IEnumerable<string> GetFilesToAddToMemory()
        {

            HashSet<string> filePathsOnMemory = new HashSet<string>(MainViewModel.ImagesViewModel.Images.Select(image => image.FilePath));
            List<string> filePathsOnDisk = Directory.EnumerateFiles(MainViewModel.DirPath, "*", SearchOption.TopDirectoryOnly)
                .Where(file => file.EndsWith(Constants.JPG, ignoreCase: true, culture: null)
                            || file.EndsWith(Constants.JPEG, ignoreCase: true, culture: null)
                            || file.EndsWith(Constants.PNG, ignoreCase: true, culture: null)
                 )
                .ToList();
            return filePathsOnDisk.Where(filePath => !filePathsOnMemory.Contains(filePath));
        }

        private static IEnumerable<MyImage> GetFilesToDeleteFromMemory()
        {
            HashSet<string> filePathsOnDisk = new HashSet<string>(Directory.EnumerateFiles(MainViewModel.DirPath, "*", SearchOption.TopDirectoryOnly)
                        .Where(file => file.EndsWith(Constants.JPG, ignoreCase: true, culture: null)
                            || file.EndsWith(Constants.JPEG, ignoreCase: true, culture: null)
                            || file.EndsWith(Constants.PNG, ignoreCase: true, culture: null)
                 ));
            List<MyImage> filePathsOnMemory = MainViewModel.ImagesViewModel.Images.ToList();
            return filePathsOnMemory.Where(image => !filePathsOnDisk.Contains(image.FilePath));
        }

        private static void AddNewImages(List<string> newFilePaths)
        {
            foreach (string filePath in newFilePaths)
            {
                App.Logger.Info($"Creating new image from disk. Path: {filePath}");
                MainViewModel.ImagesViewModel.CreateImage(filePath);
            }

            MainViewModel.ImagesViewModel.LoadThumbnails();
        }

        //Let's not delete all image data so user doesn't accidentally lose saved data but let's remove the filePath and not show the image on UI
        private static void RemoveFilePaths(List<MyImage> removedImages)
        {
            foreach (MyImage image in removedImages)
            {
                if (image.Name != null)
                {
                    App.Logger.Info($"Removing image which was removed from disk. Image: {image.FilePath}");
                    image.Name = null;
                    image.Thumbnail = null;
                }
            }
        }
    }
}
