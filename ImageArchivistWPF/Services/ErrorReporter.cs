﻿using System;

namespace ImageArchivistWPF.Services
{
    public static class ErrorReporter
    {
        private const string TO_ADDRESS = "errorreports@codeartist.fi";
        private const string SUBJECT = "Error report from Image Archivist";

        public static void SendErrorReport(string errorMessage, string descriptionFromUser, Exception exception)
        {
            string message = $"{DateTime.UtcNow} {errorMessage}\nDescription from user: {descriptionFromUser}\n\nException: {exception}";

            EmailSender.SendEmail(TO_ADDRESS, SUBJECT, message);
        }
    }
}
