﻿using System;
using System.Net;
using System.Net.Mail;

namespace ImageArchivistWPF.Services
{
    public static class EmailSender
    {
        private const string HOST = "smtp.gmail.com";
        private const int PORT = 587;
        private const string FROM_ADDRESS = "imagearchivist.errorreport@gmail.com";
        private const string PASSWORD = "@Bum5lXUTd+S";
        private const string LOGPATH = @"%USERPROFILE%\ImageArchivist\Logs\ImageArchivist_{0}.log";

        public static void SendEmail(string toAddress, string subject, string message)
        {
            MailMessage mail = new MailMessage(FROM_ADDRESS, toAddress);
            SmtpClient client = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = HOST,
                EnableSsl = true,
                Port = PORT,
                Credentials = new NetworkCredential(FROM_ADDRESS, PASSWORD)

            };
            mail.Subject = subject;
            mail.Body = message;
            try
            {
                mail.Attachments.Add(new Attachment(GetLogFilePath()));
            }
            catch
            {
                App.Logger.Error("Failed to add log file as attachment");
            }
            client.Send(mail);
        }

        private static string GetLogFilePath()
        {
            string path = String.Format(LOGPATH, DateTime.Now.ToShortDateString());
            return Environment.ExpandEnvironmentVariables(path);
        }
    }
}
