﻿namespace ImageArchivistWPF
{
    public static class Constants
    {
        public const string XML_FILE_NAME = "\\ImageArchivistData.xml";

        public const string VERSION = "0.7.5";

        //Supported file formats
        public const string JPG = ".jpg";
        public const string JPEG = ".jpeg";
        public const string PNG = ".png";
    }
}
